# Rest api
## /api/new_teams
- Type: POST
- Payload:
```json
[
    {'teamname': String},
    ...
]
``` 

# Websocket Communication
## startgame Frontend
### sends...
```json
// startgame message; creates new running game and triggers a startedgame message from backend to all clients
{
    'type': 'startgame', 
    'team_red': String, // only teams that already exist
    'team_blue': String, // only teams that already exist
    'leaderboard': String, // only leaderboards that already exist
}
```
```json
// newplayer message; creates new player and triggers a kickerdata message from backend to all clients
{
    'type': 'newplayer', 
    'name': Str,
}
```
```json
// newteam message; creates new team and triggers a kickerdata message from backend to all clients
{
    'type': 'newteam**s**', 
    'teams': [
        {
            'name': Str,
            'players': [Str, Str, Str] // only players that already exist
        }, ...
    ]
}
```
```json
// newleaderboard message; creates new leaderboard and triggers a kickerdata message from backend to all clients
{
    'type': 'newleaderboard', 
    'name': Str,
    'ldb_type': Str, // 'open'/'closed'/'tournament'
    'info': Str, // optional
    'teams': [Str, Str, Str, ...], // only teams that already exist; 'open' leaderboards get an empty List(Not restricted to any teams)
}
```
```json
// getkickerdata message; triggers a kickerdata message from backend to requesting client
{
    'type': 'getkickerdata',
}
```
### receives...
```json
// startedgame message; tells that a client started a game
    'type': 'startedgame',
    'game_data': [{'team_red': None, 'team_blue': None, 'leaderboard': None}] // additional/optional game info; not yet working
}
```
```json
// kickerdata message; contains lists of all player, team and leaderboard names (leaderboard teams not yet working)
    'type': 'kickerdata',
    'kicker_data': {'leaderboards': [{'name': Str, 'teams': [Str, Str, Str, ...]},...], 'teams': [{'name': Str, 'teams': [Str, Str, Str, ...]},...], 'players': [Str, Str, Str, ...]},
```
## runninggame Frontend
### sends...
```json
// pollstart message; creates new poll and triggers a startpoll message from backend to all clients
{
    'type': 'pollstart', 
    'poll_type': Str, // poll_type == 'endgame' or 'changescore' + 'r/b' + '+/-' + '1' e.g. 'changescoreb+1'
}
```
```json
// pollchange message; creates new poll and triggers a startpoll message from backend to all clients
{
    'type': 'pollchange', 
    'yes': Bool, 
    'no': Bool, // one of both is true, the other is false; the vote counts for the key (yes/no) that istrue
}
```
### receives...
```json
// updatescore message; contains new/changed scores
    'type': 'updatescore',
    'score_red': Int,
    'score_blue': Int,
```
```json
// startpoll message; indicates the start of a poll, contains initial poll
    'type': 'startpoll',
    'poll_data': {'type': Str, 'num_pollers': Int, 'yes': Int, 'no': Int},
```
```json
// updatepoll message; contains updated poll
    'type': 'updatepoll',
    'poll_data': {'type': Str, 'num_pollers': Int, 'yes': Int, 'no': Int},
```
```json
// endpoll message; indicates the end of a poll, contains initial poll
    'type': 'endpoll',
    'poll_type': Str, // as above...; can be used to show apropriate messages to the user
    'result': Str, // can be 'approved'/'rejected'
```