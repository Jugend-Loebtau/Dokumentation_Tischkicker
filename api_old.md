## /api/current_game/:gameKey (GET)

| Type | Description      |
| ---- | ---------------- |
| Get  | Get current Game |

### Answer

- game is running:

```json
{
  "team1": String,
  "team2": String,
  ("timer": String,)
}
```

- no game: `HTML ERROR CODE 404`

```json
{
  "message": "No games running"
}
```

- not authorized (if game_key provided): `HTML ERROR CODE 401`

```json
{
  "message": "GameKey not valid"
}
```

## /api/current_game (POST)

| Type | Description      |
| ---- | ---------------- |
| Post | Start a new Game |

- Payload:

```json
{
  "team1": String,
  "team2": String
}
```

- Answer

```json
{
  "gameKey": String
}
```

## /api/current_game (POST)

| Type | Description         |
| ---- | ------------------- |
| Put  | Update current game |

- Payload:

```json
{
  "team1": {
    "name": String,
    "score": Number
  },
  "team2": {
    "name": String,
    "score": Number
  }
}
```

- Answer

```json
{
  "gameKey": String
}
```

## /api/current_game/:gameKey

| Type   | Description                                            |
| ------ | ------------------------------------------------------ |
| DELETE | Stop a running game and save it to played games or not |

- Payload:

```json
{
  "gameKey": String,
  "save": Boolean
}
```

- Answer

```json
{
  "message": "Game successfully stopped"
}
```

## /api/games/

| Type | Description    |
| ---- | -------------- |
| Get  | Get past games |

- Answer

```json
{
  "team1": {
    "name": String,
    "score": Number
  },
  "team2": {
    "name": String,
    "score": Number
  },
  "date": Number,
  "playingTime": Number
}[]
```
