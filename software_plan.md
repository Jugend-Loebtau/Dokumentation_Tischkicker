# Tischkicker Backend

Database mit Model: tk_game

## Properties:

- ID(auto increment)
- score_red
- score_blue
- teamid_red
- teamid_blue
- running (Boolean)

Two different types of frontend:
- active:
  - only the client that connects when no game is running
  - has ability to start a new game, define team names
- passive:
  - all clients that connect while a game is running
  - only able to view the current scores

```python
views.py

def tk_game_view():
    #check if latest tk_game is running
    # true: 

def start_tk_game
```